package modelo;
public class Gasolina {
    private String IdGas;
    private String tipo;
    private float pre;
    
    public Gasolina(){
        this.pre=0;
        this.IdGas="";
        this.tipo="";
    }
    
    public Gasolina(String IdGasolina,String tipo,float precio){
        this.IdGas = IdGasolina;
        this.pre = precio;
        this.tipo = tipo;
    }
    
    public Gasolina(Gasolina gasolina){
        this.IdGas = gasolina.IdGas;
        this.pre = gasolina.pre;
        this.tipo = gasolina.tipo;
    }

    public String getIdGasolina() {
        return IdGas;
    }

    public void setIdGasolina(String IdGasolina) {
        this.IdGas = IdGasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPre() {
        return pre;
    }

    public void setPrecio(float precio) {
        this.pre = precio;
    }
}

package modelo;
public class Bomba {
    private int numDeBomba;
    private float capDeLaBomba;
    private float acumuladorDeLitros;
    private Gasolina gasolina;
    
    public Bomba(){
        this.acumuladorDeLitros=0;
        this.capDeLaBomba=0;
        this.numDeBomba=0;
        this.gasolina = new Gasolina();
    }
    
    public Bomba(int numDeBomba,float capacidadDeLaBomba,float acumuladorSeLitros,Gasolina gasolina){
        this.numDeBomba = numDeBomba;
        this.capDeLaBomba = capacidadDeLaBomba;
        this.acumuladorDeLitros = acumuladorSeLitros;
        this.gasolina = gasolina;
    }
            
    public Bomba(Bomba bomba){
        this.acumuladorDeLitros= bomba.acumuladorDeLitros;
        this.capDeLaBomba= bomba.capDeLaBomba;
        this.numDeBomba= bomba.numDeBomba;
        this.gasolina= bomba.gasolina;
    }
    public int getNumDeBomba() {
        return numDeBomba;
    }

    public void setNumDeBomba(int numDeBomba) {
        this.numDeBomba = numDeBomba;
    }

    public float getCapacidadDeLaBomba() {
        return capDeLaBomba;
    }

    public void setCapacidadDeLaBomba(float capacidadDeLaBomba) {
        this.capDeLaBomba = capacidadDeLaBomba;
    }

    public float getAcumuladorDeLitros() {
        return acumuladorDeLitros;
    }

    public void setAcumuladorSeLitros(float acumuladorSeLitros) {
        this.acumuladorDeLitros = acumuladorSeLitros;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    public void IniciarBomba(int IdBomba,Gasolina gasolina){
        this.gasolina = gasolina;
        this.capDeLaBomba = 200.0f;
        this.acumuladorDeLitros = 0.0f;
        this.numDeBomba = IdBomba;
    }
    
    public float InventarioGasolina(){
        return capDeLaBomba-acumuladorDeLitros;
    }
    
    public float VenderGasolina(float cantidad){
        this.acumuladorDeLitros += cantidad;
        return cantidad * this.gasolina.getPre();
        
    }

    public float VentasTotales(){
        return this.acumuladorDeLitros * this.gasolina.getPre();
    }
}
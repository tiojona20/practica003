package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Bomba;
import modelo.Gasolina;
import vista.dlgGasolinera;
public class Controlador implements ActionListener{
    private Gasolina gasolina;
    private Bomba bom;
    private dlgGasolinera vista;
    public Controlador(Bomba bomba,Gasolina gasolina,dlgGasolinera vista){
        this.bom =bomba;
        this.gasolina =gasolina;
        this.vista =vista;
        vista.btnCerrar.addActionListener(this);
        vista.btnInciarBomba.addActionListener(this);
        vista.btnRegistar.addActionListener(this);
        vista.cboTipo.addActionListener(this);
    }

    private void iniciarVista(){
        vista.setTitle(":: Productos ::");
        vista.setSize(590,450);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    private void limpiarTexto(){
        vista.txtTotalVenta.setText("");
        vista.txtCosto.setText("");
        vista.txtCantidad.setText("");
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.cboTipo) {
            vista.btnInciarBomba.setEnabled(true);
        }
        if(e.getSource()==vista.btnInciarBomba){
            gasolina.setIdGasolina(String.valueOf(vista.cboTipo.getSelectedIndex()));
            gasolina.setTipo(vista.cboTipo.getSelectedItem().toString());
            gasolina.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
            
            try{
                limpiarTexto();
                bom.IniciarBomba(Integer.parseInt(vista.txtNumeroDeBomba.getText()), gasolina);
                vista.sliCantidadBomba.setMaximum(200);
                vista.sliCantidadBomba.setValue(200);
                vista.txtContadorVentas.setText(String.valueOf(0));
                vista.btnRegistar.setEnabled(true);
            }catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex.getMessage());
            }
            catch(Exception ex2){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex2.getMessage());
            }
        }

        if(e.getSource()==vista.btnRegistar){
            int contador = Integer.parseInt(vista.txtContadorVentas.getText());
            try{
                float cantidad = Float.parseFloat(vista.txtCantidad.getText());
                if(cantidad <=bom.InventarioGasolina()){
                    vista.txtCosto.setText(String.valueOf(bom.VenderGasolina(cantidad)));
                    vista.txtTotalVenta.setText(String.valueOf(bom.VentasTotales()));
                    contador ++;
                    vista.txtContadorVentas.setText(String.valueOf(contador));
                    vista.sliCantidadBomba.setValue((int)bom.InventarioGasolina());
                }
                else {
                    JOptionPane.showMessageDialog(vista,"Cantidad excedida");
                }
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista,"Surgio el seguiente error:"+ ex.getMessage());
            }
        }
        
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"Seguro que quieres salir",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
    }
    
    public static void main(String[] args) {
        Bomba bomba = new Bomba();
        Gasolina gasolina = new Gasolina();
        dlgGasolinera vista = new dlgGasolinera(new JFrame(),true);
        Controlador contra = new Controlador (bomba,gasolina,vista);
        contra.iniciarVista();
    }
}
